import { combineReducers } from 'redux';
import reducerProducts from "./products/reducerProducts";
import reducerModal from "./modal/reducerModal";

const rootReducer = combineReducers({
    productsAll: reducerProducts,
    modal: reducerModal
})

export default rootReducer