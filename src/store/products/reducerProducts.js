import { GET_PRODUCTS } from './actionsProducts'

const initialState = {products: []}
const reducerProducts = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTS: {
            return {...state, products: [...action.payload]}
        }
        default: {
            return state;
        }
    }
};

export default reducerProducts