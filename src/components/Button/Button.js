import React from 'react';
import styles from './Button.module.scss';
import PropTypes from "prop-types";

const Button = (props) => {
	const { backgroundColor, handleClick, text } = props;
		return (
			<button className={styles.buttonsMain} style={{backgroundColor:backgroundColor}} onClick={handleClick}>
				{text}
			</button>
		)
}

Button.propTypes = {
	text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	backgroundColor: PropTypes.string,
	handleClick: PropTypes.func
};

Button.defaultProps = {
	backgroundColor: "#64B743",
	handleClick: () => {}
};

export default Button;